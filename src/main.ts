import Vue from 'vue'
import App from './App.vue'
import router from "@/router";
// @ts-ignore
import {ValidationProvider} from './plugins/vee-validate.js'
// @ts-ignore
import VueAwesomeSwiper from 'vue-awesome-swiper'

Vue.use(VueAwesomeSwiper)
Vue.use(require('vue-resource'));

Vue.config.productionTip = false
Vue.config.ignoredElements = [/^ion-/]
new Vue({
  components: {
    ValidationProvider
  },
  router,
  render: h => h(App),
}).$mount('#app')
