/* tslint:disable */
import Vue from 'vue'
import swiperComponent from "@/components/swiper/index.vue"
import footerComponent from "@/components/footer/index.vue"
import axios from "axios";

export default Vue.extend({
  name: "Home",
  components: {swiperComponent, footerComponent},
  data: () => ({
    arrayData: [],
  }),
  created() {
    this.getAllProduct()
  },
  methods: {
    openStart() {
      // @ts-ignore
      document.querySelector('ion-menu-controller').open('start')
    },
    async getAllProduct() {
      const {data} = await axios.post('http://importeasybuying.com/web/cliente/producto/listado?expand=foto,fotos')
      console.log(data)
      this.arrayData = data.data
    },
  }
})
