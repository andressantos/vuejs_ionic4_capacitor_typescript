/* tslint:disable */
import {CameraResultType, Plugins} from '@capacitor/core';
import Vue from 'vue'
import footerComponent from "@/components/footer/index.vue"


const {Camera, Device, Geolocation} = Plugins;

export default Vue.extend({
  name: "Home",
  components: {footerComponent},
  data: () => ({
    image: '',
  }),
  mounted() {

  },
  created() {

  },
  methods: {
    async takePicture() {
      const image = await Plugins.Camera.getPhoto({
        quality: 100,
        allowEditing: false,
        resultType: CameraResultType.Uri,
      });
      // @ts-ignore
      this.image = image.webPath;
    },

    openStart() {
      // @ts-ignore
      document.querySelector('ion-menu-controller').open('start')
    },
  }
})
