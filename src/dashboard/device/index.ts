/* tslint:disable */
import {Plugins} from '@capacitor/core';
import Vue from 'vue'
import footerComponent from "@/components/footer/index.vue"

const {Device, Geolocation} = Plugins;

export default Vue.extend({
  name: "Home",
  components: {footerComponent},
  data: () => ({
    dataDevice: null,
    coordinates: null,
    image: '',
  }),
  mounted() {
    this.getInfo()
  },

  methods: {
    async getInfo() {
      // @ts-ignore
      this.dataDevice = await Device.getInfo();
    },
    async getCurrentPosition() {
      // @ts-ignore
      this.coordinates = await Geolocation.getCurrentPosition();
    },

    openStart() {
      // @ts-ignore
      document.querySelector('ion-menu-controller').open('start')
    },


  }
})
