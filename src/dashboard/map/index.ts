/* tslint:disable */
import {Plugins} from '@capacitor/core';
import Vue from 'vue'
import footerComponent from "@/components/footer/index.vue"


const {Camera, Device, Geolocation} = Plugins;

export default Vue.extend({
  name: "Home",
  components: {footerComponent},
  data: () => ({
    image: '',
  }),
  mounted() {

  },
  created() {

  },
  methods: {
    async getCurrentPosition() {
      // @ts-ignore
      this.coordinates = await Geolocation.getCurrentPosition();
    },
    watchPosition() {
      const wait = Geolocation.watchPosition({}, (position, err) => {
      })
    },

    openStart() {
      // @ts-ignore
      document.querySelector('ion-menu-controller').open('start')
    },
  }
})
