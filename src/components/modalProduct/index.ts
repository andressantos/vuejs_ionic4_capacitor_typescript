/* tslint:disable */
import Vue from 'vue'
import {Plugins} from '@capacitor/core';

const {Modals} = Plugins;
export default Vue.extend({
  name: "modalProduct",
  props: {
    view: {
      type: Boolean, default: () => {
        return false
      }
    },
    product: {
      type: Object, default: () => {
        return null
      }
    },
  },
  created() {

  },
  data: () => ({
    arrayData: [],
    ctrlDiv: true,
  }),
  methods: {
    changeTable() {
      this.ctrlDiv = !this.ctrlDiv
    }
  }
});
// | currency({ thousandsSeparator: '.' })
