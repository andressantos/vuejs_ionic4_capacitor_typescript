import Vue from 'vue'

export default Vue.extend({
  name: 'login',
  data: () => ({}),
  methods: {
    redirectPage(url: string) {
      this.$router.push(url)
    },
  }
})
