/* tslint:disable */
import Vue from "vue";
import modal from "@/components/modalProduct/index";

export default Vue.extend({
  name: "swiperComponent",
  components: {modal},
  props: {
    type: {
      type: Boolean,
      default: () => {
        return false;
      }
    },
    arrayData: {
      type: Array,
      default: () => {
        return [];
      }
    }
  },
  data: () => ({
    viewModal: false,
    detailProduct: {},
    slideOptsCircle: {
      spaceBetween: 1,
      slidesPerView: 5,
      autoplay: false,
      loop: true
    },
    slideOptsSquare: {
      loop: true,
      spaceBetween: 1,
      slidesPerView: 3.2,
      autoplay: false,
      initialSlide: 0
    },
  }),
  methods: {
    async openModal(product: Object) {
      this.viewModal = true;
      this.detailProduct = product;
    },
  }
});

