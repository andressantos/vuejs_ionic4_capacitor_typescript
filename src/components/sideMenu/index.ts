/* tslint:disable */
import Vue from 'vue'

export default Vue.extend({
  name: "sideMenu",
  data: () => ({}),
  mounted() {

  },
  methods: {
    logOut() {
      localStorage.clear();
      this.$router.push('/')
    },
  }
})
