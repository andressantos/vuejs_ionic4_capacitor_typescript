import Router from 'vue-router'
import Vue from "vue";
import Home from '../dashboard/home/home.vue'
import Login from '../landing/login/login.vue'
import Register from '../landing/register/register.vue'
import Device from '../dashboard/device/index.vue'
import Profile from '../dashboard/profile/index.vue'
import Map from '../dashboard/map/index.vue'

Vue.use(Router);
const router = new Router({
  routes: [
    {
      path: "/",
      name: "login",
      component: Login,
    },
    {
      path: "/register",
      name: "register",
      component: Register,
    },
    {
      path: "/home",
      name: "home",
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/device",
      name: "device",
      component: Device,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/map",
      name: "map",
      component: Map,
      meta: {
        requiresAuth: true
      }
    },
  ]
});
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem('token')) {
      next()
    } else {
      next('/')
    }
  } else {
    next()
  }
});

export default router;
