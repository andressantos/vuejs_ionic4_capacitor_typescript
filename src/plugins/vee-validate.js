/* eslint-disable */
import Vue from 'vue'
import {extend, ValidationProvider} from 'vee-validate';
import {email, required} from 'vee-validate/dist/rules';

Vue.component('ValidationProvider', ValidationProvider);

extend('required', {
	...required,
	message: 'El {_field_} campo es requerido'
});
extend('email', {
	...email,
	message: 'El {_field_} campo es tipo email'
})
extend('verify_password', {
	message: field => 'La contraseña debe contener al menos: 1 letra mayúscula, 1 letra minúscula, 1 número y un carácter especial. Ejemplo: palabraA1*',
	validate: value => {
		const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*.-_])(?=.{6,})')
		return strongRegex.test(value)
	}
})


export default {ValidationProvider}
