import Vue from 'vue'
import auth from '../../services/auth/auth.services'

export default Vue.extend({
  name: 'login',
  data: () => ({
    email: '',
    password: ''
  }),
  methods: {
    async login() {
      const data: any = await auth.login(this.email, this.password)
      if (data.status === 200) {
        this.$router.push('/home')
        // this.$router.push({path: "/home"})
      }
    },
  }
})
