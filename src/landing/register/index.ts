/* eslint-disable */
/* tslint:disable */
import Vue from 'vue'
import {post} from "@/services/auth/api.services";

const swal = require('sweetalert');
export default Vue.extend({
  name: 'login',
  data: () => ({
    nombres: null,
    email: null,
    password: null
  }),
  methods: {
    async register() {
      if (this.nombres != null && this.email != null && this.password != null) {
        const {data} = await post({
          endPoint: 'auth/registro',
          userService: false,
          params: {nombres: this.nombres, email: this.email, password: this.password}
        })
        if (data.status === 200) {
          swal("Felicitaciones!", "Se ha registrado de forma correcta", "ok");
          this.$router.push('/home')
        } else {
          swal("Oops!", "Intente de nuevo", "error");
        }
      }
    }
  }
})
