/* tslint:disable */
import axios from 'axios'
import Auth from './auth.services'

const API_URL = "https://364a533d.ngrok.io/api/v1/";
// const API_URL = "http://importeasybuying.com/web/cliente/";
let header: Object = "";
let url = "";
let jsonParams = "";

// @ts-ignore
const get = async function ({endPoint, userService = false, params = {}}) {
  if (userService) {
    header = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + `${await Auth.accessParam()}`
    };
  }
  url = API_URL + endPoint;

  return new Promise(resolve => {
    axios.get(url + "?" + jsonParams, {headers: header})
      .then(data => {
        resolve(data);
      })
      .catch(error => {
      });
  });
};

// @ts-ignore
const post = async function ({endPoint, userService = false, params = {}}) {
  url = API_URL + endPoint;
  if (userService) {
    header = {
      "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: "Bearer " + `${await Auth.accessParam()}`
    };
  }
  if (Object.keys(params).length !== 0) {
    // params = await jsonToURLEncoded(params)
    return axios.post(url, params, {headers: header});
  }
  return axios.post(url, "", {headers: header});
};

function jsonToURLEncoded(jsonString: any) {
  return Object.keys(jsonString)
    .map(function (key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key])
    })
    .join('&')
}

export {post, get};
