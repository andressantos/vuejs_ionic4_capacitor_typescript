import {post} from "@/services/auth/api.services";

const swal = require('sweetalert');

let user = {}

export default {
  accessParam() {
    return new Promise(resolve => {
      resolve(localStorage.getItem('token'))
    })
  },
  setToken(token: string) {
    localStorage.setItem('token', token)
  },

  async login(email: string, password: string) {
    return new Promise(async (resolve) => {
      const {data} = await post({
        endPoint: 'auth/login',
        userService: false,
        params: {email, password}
      })
      if (data.status === 200) {
        user = data.data
        this.setToken(data.data.accessToken)
        resolve(data)
      } else {
        swal("Oops!", "usuario o contraseña incorrecta", "error");
        resolve(data)
      }
    })
  },
}
